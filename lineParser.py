#!/usr/local/bin/python3


import sys

if (len(sys.argv) != 3):
	print(">>> This tool is intended to parse each line from a file and print return all those line"
		  " that have a specific keyword in them.")
	print (">>> Arguments Missing")
	print (">>> Line Parser needs the 'Keyword' you're looking for in each line. See example below")
	print (">>> $ python lineParser.py ~/home/file.ext keyword")
	exit()

file = sys.argv[1]
keyword = sys.argv[2]
list = []

def main(file, word):
	f = open(file, "r")

	for line in f:
		line = line.rstrip()
		if word in line:
			list.append(line)
	
	f.close()

main(file, keyword)


print("==================================== START ====================================")
print(sys.argv[1])
print("===============================================================================")

for item in list:
	print (item)


print("==================================== FINISH ===================================")
print("\n \n")