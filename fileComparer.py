#!/usr/local/bin/python3


import sys
import os
from difflib import SequenceMatcher


if (len(sys.argv) != 3):
	print(">>> This tool is intended to compare two files and compare each line and return the %match value between two lines")
	print (">>> Arguments Missing")
	print (">>> File Comparer needs to know the location of pairs folder and the location of where to save the comparisons")
	print (">>> $ python3 fileComparer.py ~/home/pairs/file1.txt ~/home/destination/file2.txt")
	exit()

goodMatch=0
badMatch=0

f1 = open(sys.argv[1], "r")
f2 = open(sys.argv[2], "r")

unmatchedf1 = []
unmatchedf2 = []
matched = []

lines1 = [line for line in f1]
lines2 = [line for line in f2]

for line1 in lines1:
    if ("===" in line1) or ("MD-" in line1) or ("/Users" in line1):
        continue

    else:
        for line2 in lines2:
            if ("===" in line2) or ("MD-" in line2) or ("/Users" in line2):
                continue

            else:
                match = SequenceMatcher(None, line1, line2).ratio()
                if (match>0.7):
                    goodMatch += 1
                    print("matched: " + line2)
                    print("with: " + line1)
                    matched.append([match, line1, line2])
                    pass

                else:
                    unmatchedf2.append(line2)

        # print("Un-Matched ")
        # print(line2)
        unmatchedf1.append(line1)
        badMatch += 1

print("Good Matches:", goodMatch)
print("Bad Matches:", badMatch)
print("=================== Matched List below ====================")
# print(matched)

# print("=================== Un-Matched from File 1 below ====================")
# print(unmatchedf1)
#
# print("=================== Un-Matched from File 2 below ====================")
# print(unmatchedf2)


    #
    # compFileName =  listOfFiles[x].replace(str(pairsFolder), "")
    #
    # qTestGap = []
    # featureExtra = []
    #
    # complete = []
    #
    # comparisonFileName = comparisonFolder + "Gaps" + compFileName
    #
    #
    #
    # for qTest in qTests:
    #
    #     for feature in features:
    #
    #         match = SequenceMatcher(None, qTest, feature).ratio()
    #         if (match > 0.64):
    #             # print (qTest)
    #             # print (feature)
    #             # print (match)
    #             count += 1
    #             complete.append(qTest)
    #             complete.append(feature)
    #             if qTest in qTestGap:
    #                 qTestGap.remove(qTest)
    #
    #             if feature in featureExtra:
    #                 featureExtra.remove(feature)
    #
    #         else:
    #             if (qTest not in qTestGap) and (qTest not in complete):
    #                 qTestGap.append(qTest)
    #
    #             if (feature not in featureExtra) and (feature not in complete):
    #                 featureExtra.append(feature)
    #
    # file = open(comparisonFileName, "w")
    #
    # file.write("qTest Gaps: \n")
    # for item in qTestGap:
    #     file.write(item)
    #
    # file.write("\n")
    # file.write("\n")
    # file.write("======================================\n")
    # file.write("Feature Extras: \n")
    # for item in featureExtra:
    #     file.write(item)
    #
    # file.write("\n")
    # file.write("\n")
    # file.write("======================================\n")
    # file.write("Complete: \n")
    # for item in complete:
    #     file.write(item)
    #
    # file.write("\n")
    # file.write("\n")
    # file.write("======================================\n")
    # file.write("List of all qTests: \n")
    # for item in qTests:
    #     file.write(item)
    #
    # file.write("\n")
    # file.write("\n")
    # file.write("======================================\n")
    # file.write("List of all feature Scenarios: \n")
    # for item in features:
    #     file.write(item)
    #
    # file.close()
