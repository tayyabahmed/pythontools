

indice = "canonical_model_objects"
type = "Object"

properties = {"alias" : "string", "authority" : "string", "description" : "string", "id" : "string", "modelId" : "string", "name" : "string"}

print((properties))

def makeData(indice, type, properties={}):

    command = "uaac curl -k -XPOST https://$DSSERVER:18443/director-services/SystemServices/main?system:runTemplate=plugins/manage/com.bitstew.director-services.manage.adapters/ReadAdapterDefinition.xml -H 'Content-Type: application/xml' -d '"
    code = []
    code.append("<AdapterDefinition xmlns:emix=\"http://bitstew.com/schemas/1.0/xml-template\">")
    code.append("<Config>")
    code.append("<AdapterId>com.bitstew.qa.statusAlertXPath</AdapterId>")
    code.append("<Name>com.bitstew.qa.statusAlertXPath</Name>")
    code.append("<Description>Generates" + indice + "using MIx functions</Description>")
    code.append("<Type>" + type + "</Type>")
		<Separator>$numberOfRecords</Separator>
		<StartLine>1</StartLine>
		<RequestMethod>POST</RequestMethod>
		<BatchSize>100</BatchSize>
		<InitCode>
			<mix:variable name="numberOfRecords" select="(1 to '$NUMRECORDS')" />
			<mix:function name="randomPast_dateTime" as="xs:dateTime">
				<mix:param name="numDays" as="xs:integer"/>
				<mix:variable name="randomNumbers" select="xfn:random-integer(1,$numDays)"/>
				<mix:variable name="current" select="current-dateTime()" as="xs:dateTime"/>
				<mix:variable name="hours" select="xfn:random-integer(0,23)" />
				<mix:variable name="mins" select="xfn:random-integer(0,59)" />
				<mix:variable name="secs" select="xfn:random-integer(0,59)" />
				<mix:variable name="dates" select="$current - xdt:dayTimeDuration('\'P{\$randomNumbers}DT{\$hours}H{\$mins}M{\$secs}S\'')" />
				<mix:copy-of select="$dates - xdt:dayTimeDuration('\'-P1D\'')" />
			</mix:function>
			<mix:function name="randomFuture_dateTime" as="xs:dateTime">
				<mix:param name="numDays" as="xs:integer"/>
				<mix:variable name="randomNumbers" select="xfn:random-integer(1,$numDays)"/>
				<mix:variable name="current" select="current-dateTime()" as="xs:dateTime"/>
				<mix:variable name="hours" select="xfn:random-integer(0,23)" />
				<mix:variable name="mins" select="xfn:random-integer(0,59)" />
				<mix:variable name="secs" select="xfn:random-integer(0,59)" />
				<mix:variable name="dates" select="$current + xdt:dayTimeDuration('\'P{\$randomNumbers}DT{\$hours}H{\$mins}M{\$secs}S\'')" />
				<mix:copy-of select="$dates + xdt:dayTimeDuration('\'-P1D\'')" />
			</mix:function>
			<mix:function name="randomAlphaNumeric" as="xs:string">
				<mix:param name="AlphaNumericLength" as="xs:integer"/>
				<mix:global-variable name="ix" select="1"/>
				<mix:while test="$ix le $AlphaNumericLength">
						<mix:global-variable name="ix" select="$ix + 1"/>
						<mix:copy-of select="xfn:one-of(('\'A\',\'B\',\'C\',\'D\',\'E\',\'F\',\'G\',\'H\',\'I\',\'J\',\'K\',\'M\',\'N\',\'O\',\'P\',\'Q\',\'R\',\'S\',\'T\',\'U\',\'V\',\'W\',\'X\',\'Y\',\'Z\',\'0\',\'1\',\'2\',\'3\',\'4\',\'5\',\'6\',\'7\',\'8\',\'9\''))"/>
				</mix:while>
			</mix:function>
		</InitCode>
		<EndCode></EndCode>
		<Status>ACTIVE</Status>
		<SourceType>jdbc</SourceType>
		<SourceLocation></SourceLocation>
		<AdapterMappings>
			<Mapping>
				<AdapterId>com.bitstew.qa.statusAlertXPath</AdapterId>
				<AdapterType>StatusAlert</AdapterType>
				<URN>data.field.statusalert.id</URN>
				<DataMapping>STSALRT_{$data}</DataMapping>
				<Notes>id</Notes>
			</Mapping>
            <Mapping>
                <AdapterId>com.bitstew.qa.statusAlertXPath</AdapterId>
                <AdapterType>StatusAlert</AdapterType>
                <URN>data.field.statusalert.recordDateTime</URN>
                <DataMapping>{current-dateTime()}</DataMapping>
                <Notes>recordDateTime</Notes>
            </Mapping>
			<Mapping>
				<AdapterId>com.bitstew.qa.statusAlertXPath</AdapterId>
				<AdapterType>StatusAlert</AdapterType>
				<URN>data.field.statusalert.severity</URN>
				<DataMapping>{xfn:random-integer(1,5)}</DataMapping>
				<Notes>severity</Notes>
			</Mapping>
			<Mapping>
				<AdapterId>com.bitstew.qa.statusAlertXPath</AdapterId>
				<AdapterType>StatusAlert</AdapterType>
				<URN>data.field.statusalert.entity</URN>
				<DataMapping>{xfn:one-of(("Virtual Machine","Datacenter","Folder","Virtual App","Resource Pool","Cluster Computer Resource","Distributed Virtual Switch","Distributed Virtual Portgroup","Datastore","Network"))}</DataMapping>
				<Notes>entity</Notes>
			</Mapping>
			<Mapping>
				<AdapterId>com.bitstew.qa.statusAlertXPath</AdapterId>
				<AdapterType>StatusAlert</AdapterType>
				<URN>data.field.statusalert.category</URN>
				<DataMapping>{xfn:one-of(("A","B","C","D","E"))}</DataMapping>
				<Notes>category</Notes>
			</Mapping>
			<Mapping>
				<AdapterId>com.bitstew.qa.statusAlertXPath</AdapterId>
				<AdapterType>StatusAlert</AdapterType>
				<URN>data.field.statusalert.priority</URN>
				<DataMapping>{xfn:random-integer(1,5)}</DataMapping>
				<Notes>priority</Notes>
			</Mapping>
			<Mapping>
				<AdapterId>com.bitstew.qa.statusAlertXPath</AdapterId>
				<AdapterType>StatusAlert</AdapterType>
				<URN>data.field.statusalert.sequence</URN>
				<DataMapping>{xfn:random-integer(1,10)}</DataMapping>
				<Notes>sequence</Notes>
			</Mapping>
			<Mapping>
				<AdapterId>com.bitstew.qa.statusAlertXPath</AdapterId>
				<AdapterType>StatusAlert</AdapterType>
				<URN>data.field.statusalert.statusCode</URN>
				<DataMapping>{xfn:one-of(("dispatch","caution","assurance"))}</DataMapping>
				<Notes>statusCode</Notes>
			</Mapping>
			<Mapping>
				<AdapterId>com.bitstew.qa.statusAlertXPath</AdapterId>
				<AdapterType>StatusAlert</AdapterType>
				<URN>data.field.statusalert.entered</URN>
				<DataMapping>{randomPast_dateTime(45)}</DataMapping>
				<Notes>entered</Notes>
			</Mapping>
			<Mapping>
				<AdapterId>com.bitstew.qa.statusAlertXPath</AdapterId>
				<AdapterType>StatusAlert</AdapterType>
				<URN>data.field.statusalert.logTimes</URN>
				<DataMapping>{xfn:random-integer(1,500)}</DataMapping>
				<Notes>logTimes</Notes>
			</Mapping>
			<Mapping>
				<AdapterId>com.bitstew.qa.statusAlertXPath</AdapterId>
				<AdapterType>StatusAlert</AdapterType>
				<URN>data.field.statusalert.name</URN>
				<DataMapping>{xfn:one-of(("Potential localized leak based on consistent usage deviation","Meter read message is missing read data","Consecutive gaps in interval readings","Significant deviation in interval use","Meter read message is malformed or invalid"))}</DataMapping>
				<Notes>name</Notes>
			</Mapping>
			<Mapping>
				<AdapterId>com.bitstew.qa.statusAlertXPath</AdapterId>
				<AdapterType>StatusAlert</AdapterType>
				<URN>data.field.statusalert.updated</URN>
				<DataMapping>{randomPast_dateTime(30)}</DataMapping>
				<Notes>updated</Notes>
			</Mapping>
			<Mapping>
				<AdapterId>com.bitstew.qa.statusAlertXPath</AdapterId>
				<AdapterType>StatusAlert</AdapterType>
				<URN>data.field.statusalert.details</URN>
				<DataMapping>{xfn:one-of(("com.bitstew.test.alert.1","com.bitstew.test.alert.2","com.bitstew.test.alert.3","com.bitstew.test.alert.4","com.bitstew.test.alert.5"))}</DataMapping>
				<Notes>details</Notes>
			</Mapping>
			<Mapping>
				<AdapterId>com.bitstew.qa.statusAlertXPath</AdapterId>
				<AdapterType>StatusAlert</AdapterType>
				<URN>data.field.statusalert.state</URN>
				<DataMapping>{xfn:one-of(("ACTIVE","MUTE"))}</DataMapping>
				<Notes>state</Notes>
			</Mapping>
			<Mapping>
				<AdapterId>com.bitstew.qa.statusAlertXPath</AdapterId>
				<AdapterType>StatusAlert</AdapterType>
				<URN>data.field.statusalert.reportedBy</URN>
				<DataMapping>{concat(xs:string(xfn:one-of(("John","Adam","Berry","Carl","Drake","Evin","Felipe","Garry","Hillary","Irwin","Jose","Kent","Larry","Mary","Noam","Operah","Peter","Robert","Sara","Travis","Ulysses","Victoria","Xavier","Willian","Zack")))," ",xs:string(xfn:one-of(("John","Adam","Berry","Carl","Drake","Evin","Felipe","Garry","Hillary","Irwin","Jose","Kent","Larry","Mary","Noam","Operah","Peter","Robert","Sara","Travis","Ulysses","Victoria","Xavier","Willian","Zack"))))}</DataMapping>
				<Notes>reportedBy</Notes>
			</Mapping>
			<Mapping>
				<AdapterId>com.bitstew.qa.statusAlertXPath</AdapterId>
				<AdapterType>StatusAlert</AdapterType>
				<URN>data.field.statusalert.statusGroup</URN>
				<DataMapping>{randomAlphaNumeric(5)}</DataMapping>
				<Notes>statusGroup</Notes>
			</Mapping>
		</AdapterMappings>
	</Config>
</AdapterDefinition>'"

    return