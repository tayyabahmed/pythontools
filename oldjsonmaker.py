#!/bin/python

import sys
import os
import random as random
import string
import json
import datetime

fileLocation = "/Users/tayyabahmed/Downloads/ESDataGenerationandIngesion/DataGenerationTool/src/"

listOfFiles = (os.listdir(fileLocation))


for file in listOfFiles:
    jsonData = ""
    if file[0] == ".":
        pass
    else:
        filePath = fileLocation + file
        data = []
        print(file)

        file = open(filePath, "r")
        for line in file:
            line = line.rstrip()
            data.append(line)

        file.close()


        for line in data:
            colHead = line[0:line.find("=")]
            line = line[line.find("=")+1:]
            # print(line)
            dataType = line[0:line.find("\"")]
            if (dataType == "_RELATIVE-RANDOM-DATE_DATE_PAST_4"):
                dataType += "5"
            elif (dataType == "_RANDOM-INTEGER_1_10"):
                dataType  += "0"
            line = line.replace(dataType, "")
            # print (dataType)
            if (dataType == "_STRING-ARRAY_"):
                myData = ''.join(random.choice(string.ascii_letters) for _ in range(11))

            if (dataType == "_RANDOM-INTEGER_1_100"):

                myData = ''.join(random.choice(string.digits) for _ in range(2))
                # print(myData)

            if (dataType == "_RELATIVE-RANDOM-DATE_DATE_PAST_45"):
                # i.e. 2018-02-26 22:08:18
                myData = str(random.randrange(1990, 2019, 1))
                myData = myData + '-' + str(random.randrange(1, 12, 1))
                myData = myData + '-' + str(random.randrange(1, 28, 1))
                myData = myData + ' ' + str(random.randrange(00, 23, 1))
                myData = myData + ':' + str(random.randrange(00, 59, 1))
                myData = myData + ':' + str(random.randrange(00, 59, 1))

                # print(myData)

            jsonData += "'" + str(colHead) + "'"
            jsonData += ":'" + str(myData) + "', "
            # jsonData.append({str(colHead):str(myData)})





    print ("{" + jsonData + "}")
    print("=======================================")

# print(random.random(date))