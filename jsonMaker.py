#!/bin/python

import sys
import os
import random as random
import string
import json

import datetime
index = "adapter_configs"
type  = "AdapterConfig"
mapType = type + ".mapping"

indexLocation = "/app/mixcore/mappings/director-services/"
typeLocation =  indexLocation + index
mapTypeLocation = typeLocation + "/" + mapType

listOfIndexes = (os.listdir(indexLocation))
listOfTypes   = (os.listdir(typeLocation))

if index not in listOfIndexes:
        print("Index does not exist")
        exit()

if mapType not in listOfTypes:
        print("Type does not exist")
        exit()


dataFromFile = json.load(open(mapTypeLocation))

jsonData = "\'{ "

for colHead in (dataFromFile[type]['properties']):
    # print (colHead)
    dataTypeForCol = dataFromFile[type]['properties'][colHead]['type']
    # print(dataTypeForCol)
    myData = ""

    if dataTypeForCol == "string":
        myData = ''.join(random.choice(string.ascii_letters) for _ in range(11))

    elif dataTypeForCol == "date":
        # i.e. 2018-02-26 22:08:18
        myData = str(random.randrange(1990, 2019, 1))

        month  = str(random.randrange(1, 12, 1))
        if (len(month) == 1):
            month = "0" + month

        day = str(random.randrange(1, 28, 1))
        if (len(day) == 1):
            day = "0" + day

        hour = str(random.randrange(00, 23, 1))
        if (len(hour) == 1):
            hour = "0" + hour

        minutes = str(random.randrange(00, 59, 1))
        if (len(minutes) == 1):
            minutes = "0" + minutes

        seconds = str(random.randrange(00, 59, 1))
        if (len(seconds) == 1):
            seconds = "0" + seconds

        myData = myData + '-' + month + '-' + day + 'T' + hour + ':' + minutes + ':' + seconds



    elif dataTypeForCol == "integer":
        myData = ''.join(random.choice(string.digits) for _ in range(2))
        # print(myData)

    else:
        print("Failed Data Column " + dataTypeForCol)

    # print("Col Head: " + colHead)
    # print(myData)
    jsonData += "\"" + colHead + "\":\"" + myData + "\", "



# '{ "projectId": "boIXqZcHm", "id": "ASdfasd"}'
jsonData = jsonData[:-2] + " }\'"

print(jsonData)