# README #

# What is this repository for?

This repo is a work in progress and I have tried my best to explain each python tool available in this folder

# How do I get set up?

run the following commands

$ brew install python3

Dowload get-pip.py to ~/
https://bootstrap.pypa.io/get-pip.py

$ python3 ~/get-pip.py

### install regex package ###
$ python3 -m pip install re 

### install selenium package ###
$ python3 -m pip install selenium

### install difflib package ###
$ python3 -m pip install difflib

### install getpass package ###
$ python3 -m pip install getpass

### If you like IDEs then ###

Download PyCharm Community version from
https://www.jetbrains.com/pycharm/download/

# Contribution guidelines

Document your files

### Who do I talk to? ###

Tayyab Ahmed