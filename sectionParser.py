#!/usr/bin/python

import sys
import re as re

if (len(sys.argv) != 4):
    print (">>> Arguments Missing")
    print (">>> Section Parser needs the 'Keyword' you're looking for in each line. And")
    print (">>> It needs to know the 'start' and 'finish' points to look for")
    print (">>> This will also accept REGEX inputs enclosed inside single quotes for starting point only")
    print (">>> Example:")
    print ("$ python sectionParser.py ~/home/file.ext 'start' 'finish' ")
    print (">>> If searching for single quotes then escape with a \ ")
    print ('$ python3 sectionParser.py testHTML \'\"..-\\d\' \'\"\' > testRESULT')
    exit()

file = sys.argv[1]
start = sys.argv[2]
finish = sys.argv[3]
list = []

def instanceFind(string, keyword, n):
	string.find(keyword)

def main(file, start, finish):

    f = open(file, 'r')

    for line in f:

        line = line.rstrip()
        # print(line)

        allStart = re.findall(start, line)

        for start in allStart:
            # print (start)
            line = line[line.find(start):]

            # now find the line in the full string and append to a list
            finalLine = (line[line.find(start):line[1:].find(finish)+1])
            finalLine = finalLine.replace('"', '')
            list.append(finalLine)

            line = line[line[1:].find(finish):]

    f.close()

main(file, start, finish)

for item in list:
	print (item)
