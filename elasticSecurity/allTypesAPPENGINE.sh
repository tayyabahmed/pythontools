#!/bin/bash

# author: Tayyab Ahmed
# This script is for App Engine
# Where Roles: Admin, Power and User will be tested


echo ""
echo 'activity_records ActivityRecord ############################'
./singleTypeCheck.sh activity_records ActivityRecord admin CRUD localhost
./singleTypeCheck.sh activity_records ActivityRecord power CRUD localhost
./singleTypeCheck.sh activity_records ActivityRecord user -R-- localhost

echo ""
echo 'adapter_configs AdapterConfig ############################'
./singleTypeCheck.sh adapter_configs AdapterConfig admin CRUD localhost
./singleTypeCheck.sh adapter_configs AdapterConfig power -R-- localhost
./singleTypeCheck.sh adapter_configs AdapterConfig user ---- localhost

echo ""
echo 'adapter_mappings AdapterMapping ############################'
./singleTypeCheck.sh adapter_mappings AdapterMapping admin CRUD localhost
./singleTypeCheck.sh adapter_mappings AdapterMapping power -R-- localhost
./singleTypeCheck.sh adapter_mappings AdapterMapping user ---- localhost

echo ""
echo 'alerts Alert ############################'
./singleTypeCheck.sh alerts Alert admin CRUD localhost
./singleTypeCheck.sh alerts Alert power CRUD localhost
./singleTypeCheck.sh alerts Alert user -R-- localhost

echo ""
echo 'asset_locations AssetLocation ############################'
./singleTypeCheck.sh asset_locations AssetLocation admin CRUD localhost
./singleTypeCheck.sh asset_locations AssetLocation power CRUD localhost
./singleTypeCheck.sh asset_locations AssetLocation user ---- localhost

echo ""
echo 'assets Asset ############################'
./singleTypeCheck.sh assets Asset admin CRUD localhost
./singleTypeCheck.sh assets Asset power CRUD localhost
./singleTypeCheck.sh assets Asset user -R-- localhost

echo ""
echo 'available_data_sources AvailableDataSource ############################'
./singleTypeCheck.sh available_data_sources AvailableDataSource admin CRUD localhost
./singleTypeCheck.sh available_data_sources AvailableDataSource power -R-- localhost
./singleTypeCheck.sh available_data_sources AvailableDataSource user ---- localhost

echo ""
echo 'canonical_model_connections Connection ############################'
./singleTypeCheck.sh canonical_model_connections Connection admin CRUD localhost
./singleTypeCheck.sh canonical_model_connections Connection power -R-- localhost
./singleTypeCheck.sh canonical_model_connections Connection user ---- localhost

echo ""
echo 'canonical_model_fields Field ############################'
./singleTypeCheck.sh canonical_model_fields Field admin CRUD localhost
./singleTypeCheck.sh canonical_model_fields Field power -R-- localhost
./singleTypeCheck.sh canonical_model_fields Field user ---- localhost

echo ""
echo 'canonical_model_modelfeatures ModelFeature ############################'
./singleTypeCheck.sh canonical_model_modelfeatures ModelFeature admin CRUD localhost
./singleTypeCheck.sh canonical_model_modelfeatures ModelFeature power -R-- localhost
./singleTypeCheck.sh canonical_model_modelfeatures ModelFeature user ---- localhost

echo ""
echo 'canonical_model_objectkeys ObjectKey ############################'
./singleTypeCheck.sh canonical_model_objectkeys ObjectKey admin CRUD localhost
./singleTypeCheck.sh canonical_model_objectkeys ObjectKey power CRUD localhost
./singleTypeCheck.sh canonical_model_objectkeys ObjectKey user CRUD localhost

echo ""
echo 'canonical_model_models Model ############################'
./singleTypeCheck.sh canonical_model_models Model admin CRUD localhost
./singleTypeCheck.sh canonical_model_models Model power -R-- localhost
./singleTypeCheck.sh canonical_model_models Model user ---- localhost

echo ""
echo 'canonical_model_objects Object ############################'
./singleTypeCheck.sh canonical_model_objects Object admin CRUD localhost
./singleTypeCheck.sh canonical_model_objects Object power -R-- localhost
./singleTypeCheck.sh canonical_model_objects Object user ---- localhost

echo ""
echo 'canonical_model_packages Package ############################'
./singleTypeCheck.sh canonical_model_packages Package admin CRUD localhost
./singleTypeCheck.sh canonical_model_packages Package power -R-- localhost
./singleTypeCheck.sh canonical_model_packages Package user ---- localhost

echo ""
echo 'canonical_model_persistences Persistence ############################'
./singleTypeCheck.sh canonical_model_persistences Persistence admin CRUD localhost
./singleTypeCheck.sh canonical_model_persistences Persistence power -R-- localhost
./singleTypeCheck.sh canonical_model_persistences Persistence user ---- localhost

echo ""
echo 'com.ged_translinkbuses TranslinkBuses ############################'
./singleTypeCheck.sh com.ged_translinkbuses TranslinkBuses admin CRUD localhost
./singleTypeCheck.sh com.ged_translinkbuses TranslinkBuses power CRUD localhost
./singleTypeCheck.sh com.ged_translinkbuses TranslinkBuses user -R-- localhost

echo ""
echo 'crew_members CrewMember ############################'
./singleTypeCheck.sh crew_members CrewMember admin CRUD localhost
./singleTypeCheck.sh crew_members CrewMember power CRUD localhost
./singleTypeCheck.sh crew_members CrewMember user -R-- localhost

echo ""
echo 'customer_accounts CustomerAccount ############################'
./singleTypeCheck.sh customer_accounts CustomerAccount admin CRUD localhost
./singleTypeCheck.sh customer_accounts CustomerAccount power CRUD localhost
./singleTypeCheck.sh customer_accounts CustomerAccount user -R-- localhost

echo ""
echo 'customers Customer ############################'
./singleTypeCheck.sh customers Customer admin CRUD localhost
./singleTypeCheck.sh customers Customer power CRUD localhost
./singleTypeCheck.sh customers Customer user -R-- localhost

echo ""
echo 'data_dictionary DataDictionary ############################'
./singleTypeCheck.sh data_dictionary DataDictionary admin CRUD localhost
./singleTypeCheck.sh data_dictionary DataDictionary power -R-- localhost
./singleTypeCheck.sh data_dictionary DataDictionary user ---- localhost

echo ""
echo 'data_sources DataSource ############################'
./singleTypeCheck.sh data_sources DataSource admin CRUD localhost
./singleTypeCheck.sh data_sources DataSource power -R-- localhost
./singleTypeCheck.sh data_sources DataSource user ---- localhost

echo ""
echo 'data_sources DataSourceField ############################'
./singleTypeCheck.sh data_sources DataSourceField admin CRUD localhost
./singleTypeCheck.sh data_sources DataSourceField power -R-- localhost
./singleTypeCheck.sh data_sources DataSourceField user ---- localhost

echo ""
echo 'data_sources AvailableSource ############################'
./singleTypeCheck.sh data_sources AvailableSource admin CRUD localhost
./singleTypeCheck.sh data_sources AvailableSource power -R-- localhost
./singleTypeCheck.sh data_sources AvailableSource user ---- localhost

echo ""
echo 'environmental_alerts EnvironmentalAlert ############################'
./singleTypeCheck.sh environmental_alerts EnvironmentalAlert admin CRUD localhost
./singleTypeCheck.sh environmental_alerts EnvironmentalAlert power CRUD localhost
./singleTypeCheck.sh environmental_alerts EnvironmentalAlert user -R-- localhost

echo ""
echo 'environmental_data EnvironmentalData ############################'
./singleTypeCheck.sh environmental_data EnvironmentalData admin CRUD localhost
./singleTypeCheck.sh environmental_data EnvironmentalData power CRUD localhost
./singleTypeCheck.sh environmental_data EnvironmentalData user -R-- localhost

echo ""
echo 'environmental_events EnvironmentalEvent ############################'
./singleTypeCheck.sh environmental_events EnvironmentalEvent admin CRUD localhost
./singleTypeCheck.sh environmental_events EnvironmentalEvent power CRUD localhost
./singleTypeCheck.sh environmental_events EnvironmentalEvent user -R-- localhost

echo ""
echo 'features Key ############################'
./singleTypeCheck.sh features Key admin CRUD localhost
./singleTypeCheck.sh features Key power -R-- localhost
./singleTypeCheck.sh features Key user ---- localhost

echo ""
echo 'features Feature ############################'
./singleTypeCheck.sh features Feature admin CRUD localhost
./singleTypeCheck.sh features Feature power -R-- localhost
./singleTypeCheck.sh features Feature user ---- localhost

echo ""
echo 'features ModelingEntity ############################'
./singleTypeCheck.sh features ModelingEntity admin CRUD localhost
./singleTypeCheck.sh features ModelingEntity power -R-- localhost
./singleTypeCheck.sh features ModelingEntity user ---- localhost

echo ""
echo 'features AllConnections ############################'
./singleTypeCheck.sh features AllConnections admin CRUD localhost
./singleTypeCheck.sh features AllConnections power -R-- localhost
./singleTypeCheck.sh features AllConnections user ---- localhost

echo ""
echo 'group_role_mapping GroupRoleMapping ############################'
./singleTypeCheck.sh group_role_mapping GroupRoleMapping admin CRUD localhost
./singleTypeCheck.sh group_role_mapping GroupRoleMapping power -R-- localhost
./singleTypeCheck.sh group_role_mapping GroupRoleMapping user -R-- localhost

echo ""
echo 'help_categories HelpCategory ############################'
./singleTypeCheck.sh help_categories HelpCategory admin CRUD localhost
./singleTypeCheck.sh help_categories HelpCategory power CRUD localhost
./singleTypeCheck.sh help_categories HelpCategory user ---- localhost

echo ""
echo 'help_chapters HelpChapter ############################'
./singleTypeCheck.sh help_chapters HelpChapter admin CRUD localhost
./singleTypeCheck.sh help_chapters HelpChapter power CRUD localhost
./singleTypeCheck.sh help_chapters HelpChapter user ---- localhost

echo ""
echo 'help_topics HelpTopic ############################'
./singleTypeCheck.sh help_topics HelpTopic admin CRUD localhost
./singleTypeCheck.sh help_topics HelpTopic power CRUD localhost
./singleTypeCheck.sh help_topics HelpTopic user ---- localhost

echo ""
echo 'incidents Incident ############################'
./singleTypeCheck.sh incidents Incident admin CRUD localhost
./singleTypeCheck.sh incidents Incident power CRUD localhost
./singleTypeCheck.sh incidents Incident user -R-- localhost

echo ""
echo 'indicators Indicator ############################'
./singleTypeCheck.sh indicators Indicator admin CRUD localhost
./singleTypeCheck.sh indicators Indicator power -R-- localhost
./singleTypeCheck.sh indicators Indicator user -R-- localhost

echo ""
echo 'instrumentation_heartbeats InstrumentationHeartbeat ############################'
./singleTypeCheck.sh instrumentation_heartbeats InstrumentationHeartbeat admin CRUD localhost
./singleTypeCheck.sh instrumentation_heartbeats InstrumentationHeartbeat power -R-- localhost
./singleTypeCheck.sh instrumentation_heartbeats InstrumentationHeartbeat user ---- localhost

echo ""
echo 'jobs Job ############################'
./singleTypeCheck.sh jobs Job admin CRUD localhost
./singleTypeCheck.sh jobs Job power CRUD localhost
./singleTypeCheck.sh jobs Job user -R-- localhost

echo ""
echo 'locations Location ############################'
./singleTypeCheck.sh locations Location admin CRUD localhost
./singleTypeCheck.sh locations Location power CRUD localhost
./singleTypeCheck.sh locations Location user -R-- localhost

echo ""
echo 'mappings Mapping ############################'
./singleTypeCheck.sh mappings Mapping admin CRUD localhost
./singleTypeCheck.sh mappings Mapping power -R-- localhost
./singleTypeCheck.sh mappings Mapping user ---- localhost

echo ""
echo 'menu_items MenuItem ############################'
./singleTypeCheck.sh menu_items MenuItem admin CRUD localhost
./singleTypeCheck.sh menu_items MenuItem power -R-- localhost
./singleTypeCheck.sh menu_items MenuItem user -R-- localhost

echo ""
echo 'meter_codes MeterCode ############################'
./singleTypeCheck.sh meter_codes MeterCode admin CRUD localhost
./singleTypeCheck.sh meter_codes MeterCode power -R-- localhost
./singleTypeCheck.sh meter_codes MeterCode user ---- localhost

echo ""
echo 'meter_readings MeterReading ############################'
./singleTypeCheck.sh meter_readings MeterReading admin CRUD localhost
./singleTypeCheck.sh meter_readings MeterReading power CRUD localhost
./singleTypeCheck.sh meter_readings MeterReading user -R-- localhost

echo ""
echo 'notes Note ############################'
./singleTypeCheck.sh notes Note admin CRUD localhost
./singleTypeCheck.sh notes Note power CRUD localhost
./singleTypeCheck.sh notes Note user -R-- localhost

echo ""
echo 'notifications Notification ############################'
./singleTypeCheck.sh notifications Notification admin CRUD localhost
./singleTypeCheck.sh notifications Notification power -R-- localhost
./singleTypeCheck.sh notifications Notification user ---- localhost

echo ""
echo 'outage_records OutageRecord ############################'
./singleTypeCheck.sh outage_records OutageRecord admin CRUD localhost
./singleTypeCheck.sh outage_records OutageRecord power CRUD localhost
./singleTypeCheck.sh outage_records OutageRecord user ---- localhost

echo ""
echo 'packages Package ############################'
./singleTypeCheck.sh packages Package admin CRUD localhost
./singleTypeCheck.sh packages Package power -R-- localhost
./singleTypeCheck.sh packages Package user -R-- localhost

echo ""
echo 'plugins Plugin ############################'
./singleTypeCheck.sh plugins Plugin admin CRUD localhost
./singleTypeCheck.sh plugins Plugin power -R-- localhost
./singleTypeCheck.sh plugins Plugin user ---- localhost

echo ""
echo 'power_system_resources PowerSystemResource ############################'
./singleTypeCheck.sh power_system_resources PowerSystemResource admin CRUD localhost
./singleTypeCheck.sh power_system_resources PowerSystemResource power CRUD localhost
./singleTypeCheck.sh power_system_resources PowerSystemResource user -R-- localhost

echo ""
echo 'projects ProjectDefaultOption ############################'
./singleTypeCheck.sh projects ProjectDefaultOption admin CRUD localhost
./singleTypeCheck.sh projects ProjectDefaultOption power -R-- localhost
./singleTypeCheck.sh projects ProjectDefaultOption user ---- localhost

echo ""
echo 'projects Project ############################'
./singleTypeCheck.sh projects Project admin CRUD localhost
./singleTypeCheck.sh projects Project power -R-- localhost
./singleTypeCheck.sh projects Project user ---- localhost

echo ""
echo 'projects ProjectState ############################'
./singleTypeCheck.sh projects ProjectState admin CRUD localhost
./singleTypeCheck.sh projects ProjectState power -R-- localhost
./singleTypeCheck.sh projects ProjectState user ---- localhost

echo ""
echo 'projects ProjectDataSource ############################'
./singleTypeCheck.sh projects ProjectDataSource admin CRUD localhost
./singleTypeCheck.sh projects ProjectDataSource power -R-- localhost
./singleTypeCheck.sh projects ProjectDataSource user ---- localhost

echo ""
echo 'publication_staging GroupedObject ############################'
./singleTypeCheck.sh publication_staging GroupedObject admin CRUD localhost
./singleTypeCheck.sh publication_staging GroupedObject power -R-- localhost
./singleTypeCheck.sh publication_staging GroupedObject user ---- localhost

echo ""
echo 'reconciliation_reports ReconciliationReportEntry ############################'
./singleTypeCheck.sh reconciliation_reports ReconciliationReportEntry admin CRUD localhost
./singleTypeCheck.sh reconciliation_reports ReconciliationReportEntry power CRUD localhost
./singleTypeCheck.sh reconciliation_reports ReconciliationReportEntry user ---- localhost

echo ""
echo 'reconciliation_reports ReconciliationReport ############################'
./singleTypeCheck.sh reconciliation_reports ReconciliationReport admin CRUD localhost
./singleTypeCheck.sh reconciliation_reports ReconciliationReport power CRUD localhost
./singleTypeCheck.sh reconciliation_reports ReconciliationReport user ---- localhost

echo ""
echo 'registry_locations RegistryLocation ############################'
./singleTypeCheck.sh registry_locations RegistryLocation admin CRUD localhost
./singleTypeCheck.sh registry_locations RegistryLocation power -R-- localhost
./singleTypeCheck.sh registry_locations RegistryLocation user -R-- localhost

echo ""
echo 'registry_services RegistryService ############################'
./singleTypeCheck.sh registry_services RegistryService admin CRUD localhost
./singleTypeCheck.sh registry_services RegistryService power -R-- localhost
./singleTypeCheck.sh registry_services RegistryService user -R-- localhost

echo ""
echo 'reports Report ############################'
./singleTypeCheck.sh reports Report admin CRUD localhost
./singleTypeCheck.sh reports Report power CRUD localhost
./singleTypeCheck.sh reports Report user -R-- localhost

echo ""
echo 'roles Role ############################'
./singleTypeCheck.sh roles Role admin CRUD localhost
./singleTypeCheck.sh roles Role power -R-- localhost
./singleTypeCheck.sh roles Role user -R-- localhost

echo ""
echo 'router ErrorMessage ############################'
./singleTypeCheck.sh router ErrorMessage admin CRUD localhost
./singleTypeCheck.sh router ErrorMessage power -R-- localhost
./singleTypeCheck.sh router ErrorMessage user ---- localhost

echo ""
echo 'rule_assertions RuleAssertion ############################'
./singleTypeCheck.sh rule_assertions RuleAssertion admin CRUD localhost
./singleTypeCheck.sh rule_assertions RuleAssertion power -R-- localhost
./singleTypeCheck.sh rule_assertions RuleAssertion user -R-- localhost

echo ""
echo 'scheduler Job ############################'
./singleTypeCheck.sh scheduler Job admin CRUD localhost
./singleTypeCheck.sh scheduler Job power -R-- localhost
./singleTypeCheck.sh scheduler Job user ---- localhost

echo ""
echo 'scheduler Trigger ############################'
./singleTypeCheck.sh scheduler Trigger admin CRUD localhost
./singleTypeCheck.sh scheduler Trigger power -R-- localhost
./singleTypeCheck.sh scheduler Trigger user ---- localhost

echo ""
echo 'scheduler StatusLog ############################'
./singleTypeCheck.sh scheduler StatusLog admin CRUD localhost
./singleTypeCheck.sh scheduler StatusLog power -R-- localhost
./singleTypeCheck.sh scheduler StatusLog user ---- localhost

echo ""
echo 'service_locations ServiceLocation ############################'
./singleTypeCheck.sh service_locations ServiceLocation admin CRUD localhost
./singleTypeCheck.sh service_locations ServiceLocation power CRUD localhost
./singleTypeCheck.sh service_locations ServiceLocation user -R-- localhost

echo ""
echo 'settings setting ############################'
./singleTypeCheck.sh settings setting admin CRUD localhost
./singleTypeCheck.sh settings setting power -R-- localhost
./singleTypeCheck.sh settings setting user -R-- localhost

echo ""
echo 'source_groups SourceGroup ############################'
./singleTypeCheck.sh source_groups SourceGroup admin CRUD localhost
./singleTypeCheck.sh source_groups SourceGroup power -R-- localhost
./singleTypeCheck.sh source_groups SourceGroup user -R-- localhost

echo ""
echo 'source_groups State ############################'
./singleTypeCheck.sh source_groups State admin CRUD localhost
./singleTypeCheck.sh source_groups State power -R-- localhost
./singleTypeCheck.sh source_groups State user -R-- localhost

echo ""
echo 'status_alerts StatusAlert ############################'
./singleTypeCheck.sh status_alerts StatusAlert admin CRUD localhost
./singleTypeCheck.sh status_alerts StatusAlert power -R-- localhost
./singleTypeCheck.sh status_alerts StatusAlert user -R-- localhost

echo ""
echo 'system_activity_records SystemActivityRecord ############################'
./singleTypeCheck.sh system_activity_records SystemActivityRecord admin CRUD localhost
./singleTypeCheck.sh system_activity_records SystemActivityRecord power -R-- localhost
./singleTypeCheck.sh system_activity_records SystemActivityRecord user ---- localhost

echo ""
echo 'themes Theme ############################'
./singleTypeCheck.sh themes Theme admin CRUD localhost
./singleTypeCheck.sh themes Theme power -R-- localhost
./singleTypeCheck.sh themes Theme user -R-- localhost

echo ""
echo 'usage_point_reports UsagePointReport ############################'
./singleTypeCheck.sh usage_point_reports UsagePointReport admin CRUD localhost
./singleTypeCheck.sh usage_point_reports UsagePointReport power CRUD localhost
./singleTypeCheck.sh usage_point_reports UsagePointReport user -R-- localhost

echo ""
echo 'usage_points UsagePoint ############################'
./singleTypeCheck.sh usage_points UsagePoint admin CRUD localhost
./singleTypeCheck.sh usage_points UsagePoint power CRUD localhost
./singleTypeCheck.sh usage_points UsagePoint user -R-- localhost

echo ""
echo 'user_profiles UserProfile ############################'
./singleTypeCheck.sh user_profiles UserProfile admin CRUD localhost
./singleTypeCheck.sh user_profiles UserProfile power -R-- localhost
./singleTypeCheck.sh user_profiles UserProfile user -R-- localhost

echo ""
echo 'work_orders WorkOrder ############################'
./singleTypeCheck.sh work_orders WorkOrder admin CRUD localhost
./singleTypeCheck.sh work_orders WorkOrder power CRUD localhost
./singleTypeCheck.sh work_orders WorkOrder user -R-- localhost

echo ""
echo 'analytics.mapping.stopwords Stopwords ############################'
./singleTypeCheck.sh analytics.mapping.stopwords Stopwords admin CRUD localhost
./singleTypeCheck.sh analytics.mapping.stopwords Stopwords power CRUD localhost
./singleTypeCheck.sh analytics.mapping.stopwords Stopwords user CRUD localhost

echo ""
echo 'canonical_model_associations Association ############################'
./singleTypeCheck.sh canonical_model_associations Association admin CRUD localhost
./singleTypeCheck.sh canonical_model_associations Association power CRUD localhost
./singleTypeCheck.sh canonical_model_associations Association user CRUD localhost

echo ""
echo 'com.ged_translinkbusschedules TranslinkBusSchedules ############################'
./singleTypeCheck.sh com.ged_translinkbusschedules TranslinkBusSchedules admin CRUD localhost
./singleTypeCheck.sh com.ged_translinkbusschedules TranslinkBusSchedules power CRUD localhost
./singleTypeCheck.sh com.ged_translinkbusschedules TranslinkBusSchedules user -R-- localhost

echo ""
echo 'com.ged_translinkbusstops TranslinkBusStops ############################'
./singleTypeCheck.sh com.ged_translinkbusstops TranslinkBusStops admin CRUD localhost
./singleTypeCheck.sh com.ged_translinkbusstops TranslinkBusStops power CRUD localhost
./singleTypeCheck.sh com.ged_translinkbusstops TranslinkBusStops user -R-- localhost
