#!/bin/python

# author: Tayyab Ahmed"
# This is script meant to populate data for every index
# Must specify how many records to populate in each Index

import sys
from os import system
from subprocess import Popen, PIPE, call
import csv

chmod_command = "chmod 777 *.sh *.py"
call (chmod_command, shell=True)

if ((len(sys.argv)) != 3):
        print ("Must be used as: ./load_all_indices_with_data.py localhost 10")
        exit()


DS_SERVER = sys.argv[1]
NUM_RECORDS = sys.argv[2]



csv_content = csv.reader(open('list_of_indices.csv', 'r'))

dict={}
header=[]

i = 0
for rows in csv_content:
        if i == 0:
            header = rows
            i += 1
        else:
                dict[rows[0]] = rows[1]

# print(header[0])
# print(dict)



passed=[]
failed=[]

system("clear")
for index in dict.keys():
  type = dict[index]
  print("Now writing to " + index + " " + type)
  command = "./create.sh " + index + " " + type + " admin "+ DS_SERVER + " " + NUM_RECORDS
  # print (command)

  output = Popen(command, shell=True, stdout=PIPE, stderr=PIPE)

  output = output.communicate()[0]

  if "true" in output:
    passed.append(index + " " + type)

  else:
    failed.append(index + " " + type)

system("clear")

print ("Data successfully added to: ", passed)
print ("")
print ("")
print ("Failed to Load data to: ", failed)



