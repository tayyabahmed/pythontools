#!/bin/bash

####################################################################
#######This page is designed to update fake data to an Index #######
####################################################################


# PITFALLS OF THIS SCRIPT:
# This script will simply return "null" when updating fake data to
# the index. This was a failure, likely because of bad index name, type name
# or bad data input
# This is not an access issue as that is already handled the output word "false"

if [ "$#" -ne 4 ]; then
        echo 'NOT ENOUGH ARGUMENTS'
        echo 'There must be 5 arguments in total'
        echo 'Example Input: $ ScriptName IndexName TypeName RoleName ServerName'
        echo 'Here is another example'
        echo '$ ./update.sh features Feature user localhost'
        exit 1
fi

INDEX_NAME=$1
#echo $INDEX_NAME

TYPE_NAME=$2
#echo $TYPE_NAME

ROLE_NAME=$3
#echo $ROLE_NAME

DS_SERVER=$4
#echo $DS_SERVER

# NOW LOGIN TO THE RIGHT KIND OF ROLE
./roleLogin.sh $ROLE_NAME > /dev/null
UAA_TOKEN=`uaac context | grep access_token | awk -F": " '{print $2}'`;

# Get the total number of hits
TOTAL_HITS=`curl -k -s -H "Authorization: Bearer ${UAA_TOKEN}" -XGET "https://${DS_SERVER}:9200/${INDEX_NAME}/${TYPE_NAME}/_search" | jq -r '.hits.total'`
#echo $TOTAL_HITS

if [ "$TOTAL_HITS" -lt 3 ]; then
        # echo NOW CREATE A extra RECORDS USING ADMIN ROLE
        ./create.sh $INDEX_NAME $TYPE_NAME admin $DS_SERVER 2 > /dev/null
        TOTAL_HITS=`curl -k -s -H "Authorization: Bearer ${UAA_TOKEN}" -XGET "https://${DS_SERVER}:9200/${INDEX_NAME}/${TYPE_NAME}/_search" | jq -r '.hits.total'`
fi

UPDATE_AT=`expr $TOTAL_HITS - 1`
# echo $UPDATE_AT

if [ "$UPDATE_AT" -lt 0 ]; then
	echo null
	exit 1
fi

# Now get the ID for the newest entry
ID_TO_UPDATE=`curl -k -s -H "Authorization: Bearer ${UAA_TOKEN}" -XGET "https://${DS_SERVER}:9200/${INDEX_NAME}/${TYPE_NAME}/_search?&from=${UPDATE_AT}" | jq -r '.hits.hits[]._id'`
# echo $ID_TO_UPDATE

#DATA_OLD=`curl -k -s -H "Authorization: Bearer ${UAA_TOKEN}" -XGET "https://${DS_SERVER}:9200/${INDEX_NAME}/${TYPE_NAME}/_search?&from=${UPDATE_AT}" | jq -r '.hits.hits[]'`
#echo old data $DATA_OLD

#Now get brand new data to update at that same point
DATA=`./index_type_data_maker.py $INDEX_NAME $TYPE_NAME`
DATA_2=`./index_type_data_maker.py $INDEX_NAME $TYPE_NAME`
#echo new data $DATA

# Now update that record
UPDATE_OUTPUT=`curl -k -s -H "Authorization: Bearer ${UAA_TOKEN}" -XPOST "https://${DS_SERVER}:9200/${INDEX_NAME}/${TYPE_NAME}/${ID_TO_DELETE}/_update" -d "$DATA"`

# echo $UPDATE_OUTPUT


# Case ACCESS DENIED
if [ "$UPDATE_OUTPUT" == "Access denied" ]; then
        echo false

else

        FIRST_UPDATE_RESULT=`echo $UPDATE_OUTPUT | jq -r '._version'`
	# echo $FIRST_UPDATE_RESULT

	SECOND_UPDATE_RESULT=`curl -k -s -H "Authorization: Bearer ${UAA_TOKEN}" -XPOST "https://${DS_SERVER}:9200/${INDEX_NAME}/${TYPE_NAME}/${ID_TO_DELETE}/_update" -d "$DATA" | jq -r '._version'`
	# echo $SECOND_UPDATE_RESULT	

        # Case SUCCESS
        if [ "$FIRST_UPDATE_RESULT" -lt "$SECOND_UPDATE_RESULT" ]; then
                echo true

        # PITFall Or Case NO DATA FOUND
        else
                echo null
        fi
fi











