#!/bin/bash

####################################################################
#####This page will test a single Index and its Type's CRUD ########
####################################################################


# PITFALLS OF THIS SCRIPT:
# UNKNOWN

if [ "$#" -ne 5 ]; then
        echo 'NOT ENOUGH ARGUMENTS'
        echo 'There must be 6 arguments in total'
        echo 'Example Input: $ ScriptName IndexName TypeName RoleName RoleCRUD ServerName'
        echo 'If Create privilege is allowed use: C else -'
	echo 'If Read   privilege is allowed use: R else -'
	echo 'If Update privilege is allowed use: U else -'
	echo 'If Delete privilege is allowed use: D else -'
        echo 'Here is an example'
        echo '$ ./privCheck.sh features Feature user CR-D localhost'
        exit 1
fi

INDEX_NAME=$1
#echo $INDEX_NAME

TYPE_NAME=$2
#echo $TYPE_NAME

ROLE_NAME=$3
#echo $ROLE_NAME

ROLE_PRIV=$4
#echo $ROLE_PRIV

ROLE_C=${ROLE_PRIV:0:1}
#echo $ROLE_C

ROLE_R=${ROLE_PRIV:1:1}
#echo $ROLE_R

ROLE_U=${ROLE_PRIV:2:1}
#echo $ROLE_U

ROLE_D=${ROLE_PRIV:3:1}
#echo $ROLE_D

DS_SERVER=$5
#echo $DS_SERVER

#NOW LOGIN TO THE RIGHT KIND OF ROLE
./roleLogin.sh $ROLE_NAME > /dev/null

UAA_TOKEN=`uaac context | grep access_token | awk -F": " '{print $2}'`;

##################################################################################################################################################

#echo "******Now Trying $ROLE_NAME Create when it should be $ROLE_C *****"

# uaac context
CREATE=`./create.sh $INDEX_NAME $TYPE_NAME $ROLE_NAME $DS_SERVER 1`
#echo $CREATE

if [ "$CREATE" == 'true' ]; then

	if [ "$ROLE_C" == "C" ] || [ "$ROLE_C" == "c" ]; then
		echo $INDEX_NAME $TYPE_NAME $ROLE_NAME Create PRIV_PASS
	else
		echo $INDEX_NAME $TYPE_NAME $ROLE_NAME Create PRIV_FAIL
	fi

elif [ "$CREATE" == 'false' ]; then
	if [ "$ROLE_C" == "-" ]; then
                echo $INDEX_NAME $TYPE_NAME $ROLE_NAME Create PRIV_PASS
        else
                echo $INDEX_NAME $TYPE_NAME $ROLE_NAME Create PRIV_FAIL
        fi
else
	echo $INDEX_NAME $TYPE_NAME $ROLE_NAME Create ERROR
fi	

#echo ""

##################################################################################################################################################

#echo "******Now Trying $ROLE_NAME Read when it should be $ROLE_R *****"
# $ ./read.sh features Feature user localhost

READ=`./read.sh $INDEX_NAME $TYPE_NAME $ROLE_NAME $DS_SERVER`

if [ "$READ" == 'true' ]; then

        if [ "$ROLE_R" == "R" ] || [ "$ROLE_R" == "r" ]; then
                echo $INDEX_NAME $TYPE_NAME $ROLE_NAME Read PRIV_PASS
        else
                echo $INDEX_NAME $TYPE_NAME $ROLE_NAME Read PRIV_FAIL
        fi

elif [ "$READ" == 'false' ]; then
        if [ "$ROLE_R" == "-" ]; then
                echo $INDEX_NAME $TYPE_NAME $ROLE_NAME Read PRIV_PASS
        else
                echo $INDEX_NAME $TYPE_NAME $ROLE_NAME Read PRIV_FAIL
        fi
else
        echo $INDEX_NAME $TYPE_NAME $ROLE_NAME Read ERROR
fi

#echo ""


##################################################################################################################################################

#echo "******Now Trying $ROLE_NAME Update when it should be $ROLE_U*****"
# ./update.sh mappings Mapping admin localhost

UPDATE=`./update.sh $INDEX_NAME $TYPE_NAME $ROLE_NAME $DS_SERVER`

if [ "$UPDATE" == 'true' ]; then

        if [ "$ROLE_U" == "U" ] || [ "$ROLE_U" == "u" ]; then
                echo $INDEX_NAME $TYPE_NAME $ROLE_NAME Update PRIV_PASS
        else
                echo $INDEX_NAME $TYPE_NAME $ROLE_NAME Update PRIV_FAIL
        fi

elif [ "$UPDATE" == 'false' ]; then
        if [ "$ROLE_U" == "-" ]; then
                echo $INDEX_NAME $TYPE_NAME $ROLE_NAME Update PRIV_PASS
        else
                echo $INDEX_NAME $TYPE_NAME $ROLE_NAME Update PRIV_FAIL
        fi
else
        echo $INDEX_NAME $TYPE_NAME $ROLE_NAME Update ERROR
fi

#echo ""

##################################################################################################################################################

#echo "******Now Trying $ROLE_NAME Delete when it should be $ROLE_D*****"
# $ ./delete.sh features Feature user localhost

DELETE=`./delete.sh $INDEX_NAME $TYPE_NAME $ROLE_NAME $DS_SERVER`

if [ "$DELETE" == 'true' ]; then

        if [ "$ROLE_D" == "D" ] || [ "$ROLE_D" == "d" ]; then
                echo $INDEX_NAME $TYPE_NAME $ROLE_NAME Delete PRIV_PASS
        else
                echo $INDEX_NAME $TYPE_NAME $ROLE_NAME Delete PRIV_FAIL
        fi

elif [ "$DELETE" == 'false' ]; then
        if [ "$ROLE_D" == "-" ]; then
                echo $INDEX_NAME $TYPE_NAME $ROLE_NAME Delete PRIV_PASS
        else
                echo $INDEX_NAME $TYPE_NAME $ROLE_NAME Delete PRIV_FAIL
        fi
else
        echo $INDEX_NAME $TYPE_NAME $ROLE_NAME Delete ERROR
fi

#echo ""











