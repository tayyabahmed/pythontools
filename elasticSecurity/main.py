#!/bin/python

# author: Tayyab Ahmed
# This script is meant to test all indices for a role
# usage is: ./script_name role_name

import sys
from subprocess import call, Popen, PIPE
import csv


if ((len(sys.argv)) != 2):
        print ("Must be used as: $ ./main.py user")
        exit()



CSV_FILE = "list_of_indices.csv"
DS_SERVER = "localhost"

if DS_SERVER == "localhost":
	print "[INFO] If this is a multi-node environment, then you must use the indexer load balancer address in place of localhost inside main.py"

ROLE_NAME = sys.argv[1].lower()

chmod_command = "chmod 777 *.sh *.py"

call (chmod_command, shell=True)

try:
	csv_content = csv.reader(open(CSV_FILE, 'r'))

except Exception as e:
	print ("Unable to open CSV file " + CSV_FILE)
	exit()

data_list = []
header=[]

i = 0
for rows in csv_content:
	
	if i == 0:
	    header = rows
	    i += 1
	
	else:
		data_list.append([rows[i] for i in range(len(header))])

if ROLE_NAME not in header:
	print ("ERROR: Role " + ROLE_NAME + " not found in file " + CSV_FILE)
	exit()


index_loc = header.index("index")
type_loc = header.index("type")
role_crud_loc = header.index(ROLE_NAME)

for row in data_list:
	
	print ("##### Testing " + row[index_loc] + " " + row[type_loc] + " where " + ROLE_NAME + " can " + row[role_crud_loc] + " #####")
	
	command = "./singleTypeCheck.sh " + row[index_loc] + " " + row[type_loc] + " " + ROLE_NAME + " " + row[role_crud_loc] + " " + DS_SERVER 

	call (command, shell=True)
	print ("")
