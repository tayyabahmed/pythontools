#!/bin/bash

####################################################################
#######This page is designed to delete fake data from an Index #####
####################################################################


# PITFALLS OF THIS SCRIPT:
# This script will simply return "null" when deleteing fake data
# does not work likely because of bad index name, type name
# or other bad data input
# This is not an access issue as that is already handled

if [ "$#" -ne 4 ]; then
        echo 'NOT ENOUGH ARGUMENTS'
        echo 'There must be 5 arguments in total'
        echo 'Example Input: $ ScriptName IndexName TypeName RoleName ServerName'
        echo 'Here is another example'
        echo '$ ./delete.sh features Feature user localhost'
        exit 1
fi

INDEX_NAME=$1
#echo $INDEX_NAME

TYPE_NAME=$2
#echo $TYPE_NAME

ROLE_NAME=$3
#echo $ROLE_NAME

DS_SERVER=$4
#echo $DS_SERVER

# NOW LOGIN TO THE RIGHT KIND OF ROLE
./roleLogin.sh $ROLE_NAME > /dev/null
UAA_TOKEN=`uaac context | grep access_token | awk -F": " '{print $2}'`;

# Know how many hits you have total
READ_START_HITS=`curl -k -s -H "Authorization: Bearer ${UAA_TOKEN}" -XGET "https://${DS_SERVER}:9200/${INDEX_NAME}/${TYPE_NAME}/_search" | jq -r '.hits.total'`
#echo $READ_START_HITS

if [ "$READ_START_HITS" -eq 0 ]; then
	# echo NOW CREATE A SINGLE RECORD USING ADMIN ROLE
	./create.sh $INDEX_NAME $TYPE_NAME admin $DS_SERVER 1 > /dev/null
fi

READ_END_HITS=`curl -k -s -H "Authorization: Bearer ${UAA_TOKEN}" -XGET "https://${DS_SERVER}:9200/${INDEX_NAME}/${TYPE_NAME}/_search" | jq -r '.hits.total'`
#echo $READ_END_HITS

DELETE_AT=`expr $READ_END_HITS - 1`

#echo $DELETE_AT
# Now get the ID for the newest entry
ID_TO_DELETE=`curl -k -s -H "Authorization: Bearer ${UAA_TOKEN}" -XGET "https://${DS_SERVER}:9200/${INDEX_NAME}/${TYPE_NAME}/_search?&from=${DELETE_AT}" | jq -r '.hits.hits[]._id'`

#echo $ID_TO_DELETE
# Now delete that record
DELETE_OUTPUT=`curl -k -s -H "Authorization: Bearer ${UAA_TOKEN}" -XDELETE "https://${DS_SERVER}:9200/${INDEX_NAME}/${TYPE_NAME}/${ID_TO_DELETE}"`


# Case ACCESS DENIED
if [ "$DELETE_OUTPUT" == "Access denied" ]; then
        echo false
	
else
	
	DELETE_RESULT=`echo $DELETE_OUTPUT | jq -r '.found'`

	# Case SUCCESS
	if [ "$DELETE_RESULT" == "true" ]; then
        	echo true

	# PITFall Or Case NO DATA FOUND
	else
        	echo null
	fi
fi








