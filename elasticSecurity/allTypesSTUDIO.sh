#!/bin/bash

# author: Tayyab Ahmed
# This script is for Studio
# Where Roles: Admin, and Developer will be tested


echo ""
echo 'activity_records ActivityRecord ############################'
./singleTypeCheck.sh activity_records ActivityRecord admin CRUD localhost
./singleTypeCheck.sh activity_records ActivityRecord developer CRUD localhost

echo ""
echo 'adapter_configs AdapterConfig ############################'
./singleTypeCheck.sh adapter_configs AdapterConfig admin CRUD localhost
./singleTypeCheck.sh adapter_configs AdapterConfig developer CRUD localhost

echo ""
echo 'adapter_mappings AdapterMapping ############################'
./singleTypeCheck.sh adapter_mappings AdapterMapping admin CRUD localhost
./singleTypeCheck.sh adapter_mappings AdapterMapping developer CRUD localhost

echo ""
echo 'alerts Alert ############################'
./singleTypeCheck.sh alerts Alert admin CRUD localhost
./singleTypeCheck.sh alerts Alert developer CRUD localhost

echo ""
echo 'asset_locations AssetLocation ############################'
./singleTypeCheck.sh asset_locations AssetLocation admin CRUD localhost
./singleTypeCheck.sh asset_locations AssetLocation developer CRUD localhost

echo ""
echo 'assets Asset ############################'
./singleTypeCheck.sh assets Asset admin CRUD localhost
./singleTypeCheck.sh assets Asset developer CRUD localhost

echo ""
echo 'available_data_sources AvailableDataSource ############################'
./singleTypeCheck.sh available_data_sources AvailableDataSource admin CRUD localhost
./singleTypeCheck.sh available_data_sources AvailableDataSource developer CRUD localhost

echo ""
echo 'canonical_model_connections Connection ############################'
./singleTypeCheck.sh canonical_model_connections Connection admin CRUD localhost
./singleTypeCheck.sh canonical_model_connections Connection developer CRUD localhost

echo ""
echo 'canonical_model_fields Field ############################'
./singleTypeCheck.sh canonical_model_fields Field admin CRUD localhost
./singleTypeCheck.sh canonical_model_fields Field developer CRUD localhost

echo ""
echo 'canonical_model_modelfeatures ModelFeature ############################'
./singleTypeCheck.sh canonical_model_modelfeatures ModelFeature admin CRUD localhost
./singleTypeCheck.sh canonical_model_modelfeatures ModelFeature developer CRUD localhost

echo ""
echo 'canonical_model_objectkeys ObjectKey ############################'
./singleTypeCheck.sh canonical_model_objectkeys ObjectKey admin CRUD localhost
./singleTypeCheck.sh canonical_model_objectkeys ObjectKey developer CRUD localhost

echo ""
echo 'canonical_model_models Model ############################'
./singleTypeCheck.sh canonical_model_models Model admin CRUD localhost
./singleTypeCheck.sh canonical_model_models Model developer CRUD localhost

echo ""
echo 'canonical_model_objects Object ############################'
./singleTypeCheck.sh canonical_model_objects Object admin CRUD localhost
./singleTypeCheck.sh canonical_model_objects Object developer CRUD localhost

echo ""
echo 'canonical_model_packages Package ############################'
./singleTypeCheck.sh canonical_model_packages Package admin CRUD localhost
./singleTypeCheck.sh canonical_model_packages Package developer CRUD localhost

echo ""
echo 'canonical_model_persistences Persistence ############################'
./singleTypeCheck.sh canonical_model_persistences Persistence admin CRUD localhost
./singleTypeCheck.sh canonical_model_persistences Persistence developer CRUD localhost

echo ""
echo 'com.ged_translinkbuses TranslinkBuses ############################'
./singleTypeCheck.sh com.ged_translinkbuses TranslinkBuses admin CRUD localhost
./singleTypeCheck.sh com.ged_translinkbuses TranslinkBuses developer CRUD localhost

echo ""
echo 'crew_members CrewMember ############################'
./singleTypeCheck.sh crew_members CrewMember admin CRUD localhost
./singleTypeCheck.sh crew_members CrewMember developer CRUD localhost

echo ""
echo 'customer_accounts CustomerAccount ############################'
./singleTypeCheck.sh customer_accounts CustomerAccount admin CRUD localhost
./singleTypeCheck.sh customer_accounts CustomerAccount developer CRUD localhost

echo ""
echo 'customers Customer ############################'
./singleTypeCheck.sh customers Customer admin CRUD localhost
./singleTypeCheck.sh customers Customer developer CRUD localhost

echo ""
echo 'data_dictionary DataDictionary ############################'
./singleTypeCheck.sh data_dictionary DataDictionary admin CRUD localhost
./singleTypeCheck.sh data_dictionary DataDictionary developer CRUD localhost

echo ""
echo 'data_sources DataSource ############################'
./singleTypeCheck.sh data_sources DataSource admin CRUD localhost
./singleTypeCheck.sh data_sources DataSource developer CRUD localhost

echo ""
echo 'data_sources DataSourceField ############################'
./singleTypeCheck.sh data_sources DataSourceField admin CRUD localhost
./singleTypeCheck.sh data_sources DataSourceField developer CRUD localhost

echo ""
echo 'data_sources AvailableSource ############################'
./singleTypeCheck.sh data_sources AvailableSource admin CRUD localhost
./singleTypeCheck.sh data_sources AvailableSource developer CRUD localhost

echo ""
echo 'environmental_alerts EnvironmentalAlert ############################'
./singleTypeCheck.sh environmental_alerts EnvironmentalAlert admin CRUD localhost
./singleTypeCheck.sh environmental_alerts EnvironmentalAlert developer CRUD localhost

echo ""
echo 'environmental_data EnvironmentalData ############################'
./singleTypeCheck.sh environmental_data EnvironmentalData admin CRUD localhost
./singleTypeCheck.sh environmental_data EnvironmentalData developer CRUD localhost

echo ""
echo 'environmental_events EnvironmentalEvent ############################'
./singleTypeCheck.sh environmental_events EnvironmentalEvent admin CRUD localhost
./singleTypeCheck.sh environmental_events EnvironmentalEvent developer CRUD localhost

echo ""
echo 'features Key ############################'
./singleTypeCheck.sh features Key admin CRUD localhost
./singleTypeCheck.sh features Key developer CRUD localhost

echo ""
echo 'features Feature ############################'
./singleTypeCheck.sh features Feature admin CRUD localhost
./singleTypeCheck.sh features Feature developer CRUD localhost

echo ""
echo 'features ModelingEntity ############################'
./singleTypeCheck.sh features ModelingEntity admin CRUD localhost
./singleTypeCheck.sh features ModelingEntity developer CRUD localhost

echo ""
echo 'features AllConnections ############################'
./singleTypeCheck.sh features AllConnections admin CRUD localhost
./singleTypeCheck.sh features AllConnections developer CRUD localhost

echo ""
echo 'group_role_mapping GroupRoleMapping ############################'
./singleTypeCheck.sh group_role_mapping GroupRoleMapping admin CRUD localhost
./singleTypeCheck.sh group_role_mapping GroupRoleMapping developer CRUD localhost

echo ""
echo 'help_categories HelpCategory ############################'
./singleTypeCheck.sh help_categories HelpCategory admin CRUD localhost
./singleTypeCheck.sh help_categories HelpCategory developer CRUD localhost

echo ""
echo 'help_chapters HelpChapter ############################'
./singleTypeCheck.sh help_chapters HelpChapter admin CRUD localhost
./singleTypeCheck.sh help_chapters HelpChapter developer CRUD localhost

echo ""
echo 'help_topics HelpTopic ############################'
./singleTypeCheck.sh help_topics HelpTopic admin CRUD localhost
./singleTypeCheck.sh help_topics HelpTopic developer CRUD localhost

echo ""
echo 'incidents Incident ############################'
./singleTypeCheck.sh incidents Incident admin CRUD localhost
./singleTypeCheck.sh incidents Incident developer CRUD localhost

echo ""
echo 'indicators Indicator ############################'
./singleTypeCheck.sh indicators Indicator admin CRUD localhost
./singleTypeCheck.sh indicators Indicator developer CRUD localhost

echo ""
echo 'instrumentation_heartbeats InstrumentationHeartbeat ############################'
./singleTypeCheck.sh instrumentation_heartbeats InstrumentationHeartbeat admin CRUD localhost
./singleTypeCheck.sh instrumentation_heartbeats InstrumentationHeartbeat developer CRUD localhost

echo ""
echo 'jobs Job ############################'
./singleTypeCheck.sh jobs Job admin CRUD localhost
./singleTypeCheck.sh jobs Job developer CRUD localhost

echo ""
echo 'locations Location ############################'
./singleTypeCheck.sh locations Location admin CRUD localhost
./singleTypeCheck.sh locations Location developer CRUD localhost

echo ""
echo 'mappings Mapping ############################'
./singleTypeCheck.sh mappings Mapping admin CRUD localhost
./singleTypeCheck.sh mappings Mapping developer CRUD localhost

echo ""
echo 'menu_items MenuItem ############################'
./singleTypeCheck.sh menu_items MenuItem admin CRUD localhost
./singleTypeCheck.sh menu_items MenuItem developer CRUD localhost

echo ""
echo 'meter_codes MeterCode ############################'
./singleTypeCheck.sh meter_codes MeterCode admin CRUD localhost
./singleTypeCheck.sh meter_codes MeterCode developer CRUD localhost

echo ""
echo 'meter_readings MeterReading ############################'
./singleTypeCheck.sh meter_readings MeterReading admin CRUD localhost
./singleTypeCheck.sh meter_readings MeterReading developer CRUD localhost

echo ""
echo 'notes Note ############################'
./singleTypeCheck.sh notes Note admin CRUD localhost
./singleTypeCheck.sh notes Note developer CRUD localhost

echo ""
echo 'notifications Notification ############################'
./singleTypeCheck.sh notifications Notification admin CRUD localhost
./singleTypeCheck.sh notifications Notification developer CRUD localhost

echo ""
echo 'outage_records OutageRecord ############################'
./singleTypeCheck.sh outage_records OutageRecord admin CRUD localhost
./singleTypeCheck.sh outage_records OutageRecord developer CRUD localhost

echo ""
echo 'packages Package ############################'
./singleTypeCheck.sh packages Package admin CRUD localhost
./singleTypeCheck.sh packages Package developer CRUD localhost

echo ""
echo 'plugins Plugin ############################'
./singleTypeCheck.sh plugins Plugin admin CRUD localhost
./singleTypeCheck.sh plugins Plugin developer CRUD localhost

echo ""
echo 'power_system_resources PowerSystemResource ############################'
./singleTypeCheck.sh power_system_resources PowerSystemResource admin CRUD localhost
./singleTypeCheck.sh power_system_resources PowerSystemResource developer CRUD localhost

echo ""
echo 'projects ProjectDefaultOption ############################'
./singleTypeCheck.sh projects ProjectDefaultOption admin CRUD localhost
./singleTypeCheck.sh projects ProjectDefaultOption developer CRUD localhost

echo ""
echo 'projects Project ############################'
./singleTypeCheck.sh projects Project admin CRUD localhost
./singleTypeCheck.sh projects Project developer CRUD localhost

echo ""
echo 'projects ProjectState ############################'
./singleTypeCheck.sh projects ProjectState admin CRUD localhost
./singleTypeCheck.sh projects ProjectState developer CRUD localhost

echo ""
echo 'projects ProjectDataSource ############################'
./singleTypeCheck.sh projects ProjectDataSource admin CRUD localhost
./singleTypeCheck.sh projects ProjectDataSource developer CRUD localhost

echo ""
echo 'publication_staging GroupedObject ############################'
./singleTypeCheck.sh publication_staging GroupedObject admin CRUD localhost
./singleTypeCheck.sh publication_staging GroupedObject developer CRUD localhost

echo ""
echo 'reconciliation_reports ReconciliationReportEntry ############################'
./singleTypeCheck.sh reconciliation_reports ReconciliationReportEntry admin CRUD localhost
./singleTypeCheck.sh reconciliation_reports ReconciliationReportEntry developer CRUD localhost

echo ""
echo 'reconciliation_reports ReconciliationReport ############################'
./singleTypeCheck.sh reconciliation_reports ReconciliationReport admin CRUD localhost
./singleTypeCheck.sh reconciliation_reports ReconciliationReport developer CRUD localhost

echo ""
echo 'registry_locations RegistryLocation ############################'
./singleTypeCheck.sh registry_locations RegistryLocation admin CRUD localhost
./singleTypeCheck.sh registry_locations RegistryLocation developer CRUD localhost

echo ""
echo 'registry_services RegistryService ############################'
./singleTypeCheck.sh registry_services RegistryService admin CRUD localhost
./singleTypeCheck.sh registry_services RegistryService developer CRUD localhost

echo ""
echo 'reports Report ############################'
./singleTypeCheck.sh reports Report admin CRUD localhost
./singleTypeCheck.sh reports Report developer CRUD localhost

echo ""
echo 'roles Role ############################'
./singleTypeCheck.sh roles Role admin CRUD localhost
./singleTypeCheck.sh roles Role developer CRUD localhost

echo ""
echo 'router ErrorMessage ############################'
./singleTypeCheck.sh router ErrorMessage admin CRUD localhost
./singleTypeCheck.sh router ErrorMessage developer CRUD localhost

echo ""
echo 'rule_assertions RuleAssertion ############################'
./singleTypeCheck.sh rule_assertions RuleAssertion admin CRUD localhost
./singleTypeCheck.sh rule_assertions RuleAssertion developer CRUD localhost

echo ""
echo 'scheduler Job ############################'
./singleTypeCheck.sh scheduler Job admin CRUD localhost
./singleTypeCheck.sh scheduler Job developer CRUD localhost

echo ""
echo 'scheduler Trigger ############################'
./singleTypeCheck.sh scheduler Trigger admin CRUD localhost
./singleTypeCheck.sh scheduler Trigger developer CRUD localhost

echo ""
echo 'scheduler StatusLog ############################'
./singleTypeCheck.sh scheduler StatusLog admin CRUD localhost
./singleTypeCheck.sh scheduler StatusLog developer CRUD localhost

echo ""
echo 'service_locations ServiceLocation ############################'
./singleTypeCheck.sh service_locations ServiceLocation admin CRUD localhost
./singleTypeCheck.sh service_locations ServiceLocation developer CRUD localhost

echo ""
echo 'settings setting ############################'
./singleTypeCheck.sh settings setting admin CRUD localhost
./singleTypeCheck.sh settings setting developer CRUD localhost

echo ""
echo 'source_groups SourceGroup ############################'
./singleTypeCheck.sh source_groups SourceGroup admin CRUD localhost
./singleTypeCheck.sh source_groups SourceGroup developer CRUD localhost

echo ""
echo 'source_groups State ############################'
./singleTypeCheck.sh source_groups State admin CRUD localhost
./singleTypeCheck.sh source_groups State developer CRUD localhost

echo ""
echo 'status_alerts StatusAlert ############################'
./singleTypeCheck.sh status_alerts StatusAlert admin CRUD localhost
./singleTypeCheck.sh status_alerts StatusAlert developer CRUD localhost

echo ""
echo 'system_activity_records SystemActivityRecord ############################'
./singleTypeCheck.sh system_activity_records SystemActivityRecord admin CRUD localhost
./singleTypeCheck.sh system_activity_records SystemActivityRecord developer CRUD localhost

echo ""
echo 'themes Theme ############################'
./singleTypeCheck.sh themes Theme admin CRUD localhost
./singleTypeCheck.sh themes Theme developer CRUD localhost

echo ""
echo 'usage_point_reports UsagePointReport ############################'
./singleTypeCheck.sh usage_point_reports UsagePointReport admin CRUD localhost
./singleTypeCheck.sh usage_point_reports UsagePointReport developer CRUD localhost

echo ""
echo 'usage_points UsagePoint ############################'
./singleTypeCheck.sh usage_points UsagePoint admin CRUD localhost
./singleTypeCheck.sh usage_points UsagePoint developer CRUD localhost

echo ""
echo 'user_profiles UserProfile ############################'
./singleTypeCheck.sh user_profiles UserProfile admin CRUD localhost
./singleTypeCheck.sh user_profiles UserProfile developer CRUD localhost

echo ""
echo 'work_orders WorkOrder ############################'
./singleTypeCheck.sh work_orders WorkOrder admin CRUD localhost
./singleTypeCheck.sh work_orders WorkOrder developer CRUD localhost

echo ""
echo 'analytics.mapping.stopwords Stopwords ############################'
./singleTypeCheck.sh analytics.mapping.stopwords Stopwords admin CRUD localhost
./singleTypeCheck.sh analytics.mapping.stopwords Stopwords developer CRUD localhost

echo ""
echo 'canonical_model_associations Association ############################'
./singleTypeCheck.sh canonical_model_associations Association admin CRUD localhost
./singleTypeCheck.sh canonical_model_associations Association developer CRUD localhost

echo ""
echo 'com.ged_translinkbusschedules TranslinkBusSchedules ############################'
./singleTypeCheck.sh com.ged_translinkbusschedules TranslinkBusSchedules admin CRUD localhost
./singleTypeCheck.sh com.ged_translinkbusschedules TranslinkBusSchedules developer CRUD localhost

echo ""
echo 'com.ged_translinkbusstops TranslinkBusStops ############################'
./singleTypeCheck.sh com.ged_translinkbusstops TranslinkBusStops admin CRUD localhost
./singleTypeCheck.sh com.ged_translinkbusstops TranslinkBusStops developer CRUD localhost
