#!/bin/bash

####################################################################
#######This page is designed to write fake data to an Index ########
####################################################################


# PITFALLS OF THIS SCRIPT:
# This script will simply return "null" if adding fake data to
# the index was a failure likely because of bad index name, type name
# or bad data input
# This is not an access issue as that is already handled

if [ "$#" -ne 5 ]; then
        echo 'NOT ENOUGH ARGUMENTS'
        echo 'There must be 6 arguments in total'
        echo 'Example Input: $ ScriptName IndexName TypeName RoleName ServerName NumberOfRecordsToCreate'
        echo '$ ./create.sh features Feature admin localhost 10'
        exit 1
fi

INDEX_NAME=$1
#echo $INDEX_NAME

TYPE_NAME=$2
#echo $TYPE_NAME

ROLE_NAME=$3
#echo $ROLE_NAME

DS_SERVER=$4

NUM_RECORDS=$5

./roleLogin.sh $ROLE_NAME > /dev/null

UAA_TOKEN=`uaac context | grep access_token | awk -F": " '{print $2}'`;


for i in `seq 1 $NUM_RECORDS`
do
        DATA=`./index_type_data_maker.py $INDEX_NAME $TYPE_NAME`

        RESULT=`curl -k -s -H "Authorization: Bearer $UAA_TOKEN" -XPOST "https://$DS_SERVER:9200/$INDEX_NAME/$TYPE_NAME" -d "$DATA"`
	
	#echo $RESULT
	
	# Case ACCESS DENIED
	if [ "$RESULT" == "Access denied" ]; then
		echo false
	
	else
		CREATE=`echo $RESULT | jq '.created'`
		
		# Case FAIL, creation was a failure un-related to access issue likely an error in inputs
	        if [ "$CREATE" != 'true' ]; then
			#PITFALL
        	        echo null
			# echo $RESULTS

		# Case SUCCESS
		else
                	echo $CREATE
		fi
        fi
done


