#!/bin/bash

####################################################################
#####This page will create a new Role ID and Get the UAA Token######
####################################################################


# PITFALLS OF THIS SCRIPT:
# uaac target is currently hard coded
# the admin user and password are also hard coded

if [ "$#" -ne 1 ]; then
        echo 'NOT ENOUGH ARGUMENTS'
        echo 'There must be 2 arguments in total'
        echo 'Example Input: $ ScriptName RoleName'
        echo 'Here is another example'
        echo '$ ./roleLogin.sh user'
        exit 1
fi

ROLE_TYPE=$1
UNIQUE=`echo $ENV_ID | awk -F - '{ print $1 $2 $3 }'`
#echo $UNIQUE
ROLE_USER_NAME="$UNIQUE$ROLE_TYPE"


uaac target $UAA_URL

# PITFALL
uaac token client get admin -s vijay123;
# uaac token client get admin -s Pr3dixApg$;

uaac client add $ROLE_USER_NAME -s secret --authorities "${ENV_ID}.${ROLE_TYPE}" --authorized_grant_types "client_credentials refresh_token";

uaac token client get $ROLE_USER_NAME -s secret;

export UAA_TOKEN=`uaac context | grep access_token | awk -F": " '{print $2}'`;



