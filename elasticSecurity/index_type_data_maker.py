#!/bin/python


####################################################################
#######This page will create random data for an Index & Type########
####################################################################
##Must be used as: ./index_type_data_maker.py index_name TypeName###
####################################################################


# PITFALLS OF THIS SCRIPT:
# This script does not generate data for all data types
# Currently handling
# string, date, integer, double, float, boolean, ip, geopoint
# ALL OTHER DATA TYPES ARE SKIPPED
# ANOTHER PITFALL
# The location of data mappings is hard coded, 
# variable indexLocation must be changed if the location changes

import sys
import os
import random as random
from subprocess import call, Popen, PIPE
import string
import json

user = Popen('whoami', shell=True, stdout=PIPE, stderr=PIPE)

user = str(user.communicate()[0]).replace('\n', '')

if user != "root":
    raise Exception("User must be a root user to get Index Mappings, current user is", user)

if ((len(sys.argv)) != 3):
	raise Exception("Must be used as: ./index_type_data_maker.py index_name TypeName")

import datetime
index = sys.argv[1]
type  = sys.argv[2]
mapType = type + ".mapping"

# PITFALL
indexLocation = "/usr/local/mixcore/mappings/director-services/"

typeLocation =  indexLocation + index
mapTypeLocation = typeLocation + "/" + mapType

listOfIndexes = (os.listdir(indexLocation))

try:
	listOfTypes   = (os.listdir(typeLocation))
except Exception as e:
	print("Index: " + sys.argv[1] + " NOT FOUND")
	exit()

if mapType not in listOfTypes:
        print("Type " + mapType + " NOT FOUND")
        exit()


dataFromFile = json.load(open(mapTypeLocation))

jsonData = "{ "
# print (dataFromFile)

for colHead in (dataFromFile[type]['properties']):
    # print (colHead)
    dataTypeForCol = dataFromFile[type]['properties'][colHead]['type']
    # print(dataTypeForCol)
    num_properties = (len(dataFromFile[type]['properties'][colHead]))    
    myData = ""

    if (dataTypeForCol == "string") and (num_properties > 1):
        myData = ''.join(random.choice(string.ascii_letters) for _ in range(4))

    elif (dataTypeForCol == "date") and (num_properties > 1):
        # i.e. 2018-02-26 22:08:18
        myData = str(random.randrange(1990, 2019, 1))

        month  = str(random.randrange(1, 12, 1))
        if (len(month) == 1):
            month = "0" + month

        day = str(random.randrange(1, 28, 1))
        if (len(day) == 1):
            day = "0" + day

        hour = str(random.randrange(00, 23, 1))
        if (len(hour) == 1):
            hour = "0" + hour

        minutes = str(random.randrange(00, 59, 1))
        if (len(minutes) == 1):
            minutes = "0" + minutes

        seconds = str(random.randrange(00, 59, 1))
        if (len(seconds) == 1):
            seconds = "0" + seconds

        myData = myData + '-' + month + '-' + day + 'T' + hour + ':' + minutes + ':' + seconds



    elif (dataTypeForCol == "integer") and (num_properties > 1):
        myData = ''.join(random.choice(string.digits) for _ in range(2))
        # print(myData)

    elif (dataTypeForCol == "float") or (dataTypeForCol == "double") and (num_properties > 1):
        myData = ''.join(random.choice(string.digits) for _ in range(2))
	myData = myData +'.' + ''.join(random.choice(string.digits) for _ in range(2))
        # print(myData)
    
    elif (dataTypeForCol == "boolean") and (num_properties > 1):
 	myData = random.choice(['1', '0'])
	# myData = ''
        # print(myData)

    elif (dataTypeForCol == "ip") and (num_properties > 1):
	myData = str(random.randrange(10, 199, 1)) + '.'
	myData += str(random.randrange(10, 199, 1)) + '.'
	myData += str(random.randrange(10, 199, 1)) + '.'
	myData += str(random.randrange(10, 199, 1))

    elif (dataTypeForCol == "geo_point") and (num_properties > 1):
	myData = ''.join(random.choice(string.digits) for _ in range(2))
        myData += '.' + ''.join(random.choice(string.digits) for _ in range(2))
	myData += ', ' + ''.join(random.choice(string.digits) for _ in range(2))
	myData += '.' + ''.join(random.choice(string.digits) for _ in range(2))

    else:
	#myData = ''.join(random.choice(string.ascii_letters) for _ in range(3))
        #print("Failed Data Column: " + colHead + " with Type: " +dataTypeForCol)
	myData = ""

    # print("Col Head: " + colHead)
    # print(myData)
    if myData == "":
	pass
    else:
	jsonData += "\"" + colHead + "\":\"" + myData + "\", "


jsonData = jsonData[:-2] + " }"

print(jsonData)
