#!/bin/bash

####################################################################
#######This page is designed to read data from an Index/Type########
####################################################################


# PITFALLS OF THIS SCRIPT:
# This script does not fully handle issues when a wrong input is given
# i.e. if a bad index name, or a bad type name is provided, the 
# script will simply return null
# This script also does not handle empty Indices, if your index has
# zero amount of data in it, the script will still return null
# You must add data to your index first!!


if [ "$#" -ne 4 ]; then
        echo 'NOT ENOUGH ARGUMENTS'
        echo 'There must be 5 arguments in total'
        echo 'Example Input: $ ScriptName IndexName TypeName RoleName ServerName'
        echo 'Here is another example'
        echo '$ ./read.sh features Feature user localhost'
        exit 1
fi

INDEX_NAME=$1
#echo $INDEX_NAME

TYPE_NAME=$2
#echo $TYPE_NAME

ROLE_NAME=$3
#echo $ROLE_NAME

DS_SERVER=$4
#echo $DS_SERVER

#NOW LOGIN TO THE RIGHT KIND OF ROLE
./roleLogin.sh $ROLE_NAME > /dev/null

UAA_TOKEN=`uaac context | grep access_token | awk -F": " '{print $2}'`;

READ=`curl -k -s -H "Authorization: Bearer ${UAA_TOKEN}" -XGET "https://${DS_SERVER}:9200/${INDEX_NAME}/${TYPE_NAME}/_search"`
READ_TOTAL=`echo $READ | jq -r '.hits.total'`
#echo $READ
#echo $READ_TOTAL

# Case ACCESS DENIED
if [ "$READ" == "Access denied" ]; then
        echo false

# Case NO DATA AVAILABLE
elif [ "$READ_TOTAL" -eq 0 ]; then
	echo null

# Case SUCCESS
elif [ "$READ_TOTAL" -gt 0 ]; then
	echo true

else
	echo null
	#PITFALL
	#echo $READ
	#echo $READ_TOTAL
fi
