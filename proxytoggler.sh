#!/usr/bin/env bash

# author: Sanjida Minar

numbOfProc=$(ps aux | grep 'MyAppsAnywhere' | wc -l)
Adapters=( 'Wi-Fi' 'Targus USB3.0 DV Docking Statio' 'USB 10/100/1000 LAN' )
switchOn () {
  open -a "MyAppsAnywhere"
}
switchOff () {
  for (( i = 0; i < ${#Adapters[@]}; i++ )); do
    networksetup -setautoproxystate "${Adapters[$i]}" off
    networksetup -getautoproxyurl "${Adapters[$i]}" | grep 'Enabled'
  done
  # close myapps
  pkill -9 "MyAppsAnywhere"
  pkill -9 "mactunnel"
}
if [[ -z $1 ]]; then
  if [[ $numbOfProc -lt 3 ]]; then
    # enable proxy
    switchOn
  elif [[ $numbOfProc -ge 3 ]]; then
    # disable proxy
    switchOff
  fi
fi

